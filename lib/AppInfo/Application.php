<?php

namespace OCA\eCloudLauncher\AppInfo;

use OCP\AppFramework\App;

class Application extends App
{
    public function __construct(array $urlParams = array())
    {
        $appName = "ecloud-launcher";
        parent::__construct($appName, $urlParams);
    }
}
