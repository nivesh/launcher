<?php

namespace OCA\eCloudLauncher;

use OCP\IConfig;
use OCP\INavigationManager;
use OCP\App\IAppManager;
use OCP\L10N\IFactory;
use OCP\Files\IRootFolder;
use OCP\Files\Folder;

class Util
{

    private $appName;
    private $userId;
    private $config;
    private $navigationManager;
    private $appManager;
    private $l10nFac;
    private $root;

    private const DEFAULT_ORDER = array("/apps/files/", "/apps/rainloop/", "/apps/contacts/", "/apps/calendar/", "/apps/notes/", "/apps/tasks/", "/apps/photos/");
    public function __construct(
        $appName,
        IConfig $config,
        INavigationManager $navigationManager,
        IAppManager $appManager,
        IFactory $l10nFac,
        IRootFolder $root,
        $userId
    ) {
        $this->appName = $appName;
        $this->userId = $userId;
        $this->config = $config;
        $this->navigationManager = $navigationManager;
        $this->appManager = $appManager;
        $this->l10nFac = $l10nFac;
        $this->root = $root;
    }

    private function getOnlyOfficeEntries()
    {
        $l = $this->l10nFac->get("onlyoffice");
        $onlyOfficeEntries = array(
            array(
                "id" => "onlyoffice_docx",
                "icon" => "/svg/core/filetypes/x-office-document",
                "name" => $l->t("Document"),
            ),
            array(
                "id" => "onlyoffice_xlsx",
                "icon" => "/svg/core/filetypes/x-office-spreadsheet",
                "name" => $l->t("Spreadsheet"),
            ),
            array(
                "id" => "onlyoffice_pptx",
                "icon" => "/svg/core/filetypes/x-office-presentation",
                "name" => $l->t("Presentation"),
            ),
        );
        $onlyOfficeEntries = array_map(function ($entry) {
            $entry["type"] = "onlyoffice";
            $entry["active"] = false;
            $entry["href"] = "/apps/onlyoffice/ajax/new";
            return $entry;
        }, $onlyOfficeEntries);

        return $onlyOfficeEntries;
    }

    public function getOrder()
    {
        $order_raw = $this->config->getUserValue($this->userId, $this->appName, 'order');
        // If order raw empty try to get from 'apporder' app config
        $order_raw = !$order_raw ? $this->config->getUserValue($this->userId, 'apporder', 'order') : $order_raw;
        // If order raw is still empty, return empty array
        if (!$order_raw) {
            return self::DEFAULT_ORDER;
        }
        $order = json_decode($order_raw);
        return $order;
    }
    public function getAppEntries()
    {
        $entries = array_values($this->navigationManager->getAll());
        $order = $this->getOrder();
        $external = array();
        $entriesByHref = array();

        // Set icon for each app entry to use svg controller to ensure correct icon is returned
        // If not an external app, add to array indexed by "href"
        // Else add to "external" array
        foreach ($entries as &$entry) {
            if (strpos($entry["id"], "external_index") !== 0) {
                $iconName = basename($entry["icon"]);
                $iconName = preg_split('/.svg/', $iconName)[0];
                $entry["icon"] = "/svg/" . $entry["id"] . "/" . $iconName;
            }
            // Add "y" axis icon offset to certain icons
            $entry["iconOffsetY"] = 0;

            if (strpos($entry["id"], "external_index") !== 0) {
                $entriesByHref[$entry["href"]] = $entry;
            } else {
                $external[] = $entry;
            }
        }

        /*
         Sort apps according to order
         Since "entriesByHref" is indexed by "href", simply reverse the order array and prepend in "entriesByHref"
         Prepend is done by using each "href" in the reversed order array and doing a union of the "entriesByHref"
         array with the current element
        */
        if ($order) {
            $order = array_reverse($order);
            foreach ($order as $href) {
                if (!empty($entriesByHref[$href])) {
                    $entriesByHref = array($href => $entriesByHref[$href]) + $entriesByHref;
                }
            }
        }

        if ($this->appManager->isEnabledForUser("onlyoffice")) {
            $office_entries = $this->getOnlyOfficeEntries();
            $external = array_merge($office_entries, $external);
        }
        // Call array_values again to make sure keys are sequential, else json_encode
        // converts it to a JSON object in the response
        return array("apps" => array_values($entriesByHref), "external" => $external);
    }

    public function getDocumentsFolder($name)
    {
        $l = $this->l10nFac->get("onlyoffice");
        $userPath = $this->root->getUserFolder($this->userId)->getPath();
        $filePath = $userPath . '/' . $name;

        if ($this->root->nodeExists($filePath)) {
            $folder = $this->root->get($filePath);
        } else {
            $folder = $this->root->get($userPath);
            $filePath = $userPath;
        }

        if (!($folder instanceof Folder)) {
            $error = $l->t('The required folder was not found');
            return array('error' => $error, 'status' => 404);
        }

        $dir = $filePath === $userPath ? '/' : $folder->getName();
        return array('dir' => $dir, 'status' => 200);
    }

    public function updateOrder(string $order)
    {
        $this->config->setUserValue($this->userId, $this->appName, 'order', $order);
    }
}
