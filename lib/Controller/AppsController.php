<?php

namespace OCA\eCloudLauncher\Controller;

use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\JSONResponse;
use OCP\IRequest;
use OCA\eCloudLauncher\Util;

class AppsController extends Controller
{

    private $util;

    public function __construct(
        $appName,
        IRequest $request,
        Util $util
    ) {
        parent::__construct($appName, $request);
        $this->util = $util;
    }
    /**
     * @NoAdminRequired
     * @return JSONResponse
     */
    public function getOrder()
    {
        $response = new JSONResponse();
        $response->setData(array("order" => $this->util->getOrder()));
        return $response;
    }
    /**
     * @NoAdminRequired
     * @return JSONResponse
     */
    public function index()
    {
        $response = new JSONResponse();
        $entries = $this->util->getAppEntries();
        $response->setData($entries);
        return $response;
    }

    /**
     *  @NoAdminRequired
     * @return JSONResponse
     */

    public function getDocumentsFolder()
    {
        $response = new JSONResponse();
        $folderInfo = $this->util->getDocumentsFolder("Documents");
        if ($folderInfo["status"] !== 200) {
            $response->setStatus($folderInfo["status"]);
            $response->setData(array('error' => $folderInfo['error']));
        } else {
            $response->setData(array('dir' => $folderInfo['dir']));
        }

        return $response;
    }
    /**
     * @NoAdminRequired
     */

    public function updateOrder(string $order)
    {
        $this->util->updateOrder($order);
    }
}
