## /e/ Cloud Launcher App
- Adds a launcher accessible by clicking on the logo
- Recommended to use with  the /e/ Cloud theme 
- App order sorting in the Navbar is based on ["apporder" App](https://github.com/juliushaertl/apporder/). Thanks to developer at github "juliushaertl" for the work on this.