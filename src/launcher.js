import Vue from "vue";
import Launcher from "./Launcher.vue";
import { generateUrl } from "@nextcloud/router";
import axios from "@nextcloud/axios";

Vue.prototype.t = t;
Vue.prototype.OC = OC;

function showLauncher() {
	$("#launcher-menu").show();
}

function hideLauncher() {
	$("#launcher-menu").hide();
}

function mountLauncherComponent() {
	const launcherContainer = document.querySelector("#launcher-menu");

	if (launcherContainer) {
		const View = Vue.extend(Launcher);
		const launcher = new View({});

		launcher.$mount("#launcher-menu");
		hideLauncher();
	} else {
		window.setTimeout(mountLauncherComponent, 50);
	}
}

function mapMenu(parent, order, hidden) {
	const availableApps = {};
	parent.find("li").each(function() {
		const id = $(this)
			.find("a")
			.attr("href");
		const appId = $(this).attr("data-id");
		const elementId = $(this).attr("id");

		// If app is to be hidden or
		// if app is an external site
		// remove from navbar(skip the "more apps" menu li)
		if (
			hidden.indexOf(id) > -1 ||
			(elementId !== "more-apps" && appId.indexOf("external_index") === 0)
		) {
			$(this).remove();
		}
		availableApps[id] = $(this);
	});

	// Remove hidden from order array
	order = order.filter(function(e) {
		return !(hidden.indexOf(e) > -1);
	});
	$.each(order, function(order, value) {
		parent.prepend(availableApps[value]);
	});
}

// Hides the "More" icon and shows only first n entries
function showNavbarEntries(toShow) {
	// Hide more menu entry
	$("#appmenu #more-apps").addClass("hidden-by-launcher");
	// Hide app entries with index + 1 > toShow
	// Show app entries already hidden if index+1 <= toShow
	$("#appmenu li").each(function(index, element) {
		if (toShow < index + 1) {
			$(element).addClass("hidden-by-launcher");
		} else {
			// To handle resizing of browser window - from small to large
			$(element).removeClass("hidden-by-launcher");
		}
	});

	// Remove the in-header class from the more menu entries
	$("#navigation #apps ul > li").each(function(index, element) {
		if (toShow < index + 1) {
			$(element).removeClass("in-header");
		}
	});
}

function insertEntry(barEntries, menuEntries, oldIndex, newIndex) {
	const $entryToShiftInBar = $(barEntries[newIndex]);
	const $entryToInsertInBar = $(barEntries[oldIndex]);
	const $entryToShiftInMenu = $(menuEntries[newIndex]);
	const $entryToInsertInMenu = $(menuEntries[oldIndex]);
	const menuToBar =
		$entryToInsertInBar.hasClass("hidden-by-launcher") &&
		!$entryToShiftInBar.hasClass("hidden-by-launcher");
	const barToMenu =
		!$entryToInsertInBar.hasClass("hidden-by-launcher") &&
		$entryToShiftInBar.hasClass("hidden-by-launcher");

	let firstHiddenIndex = -1;
	const totalEntries = barEntries.length;
	for (let index = 0; index < totalEntries; index++) {
		if ($(barEntries[index]).hasClass("hidden-by-launcher")) {
			firstHiddenIndex = index;
			break;
		}
	}

	if (menuToBar) {
		const toHideInBar = barEntries[firstHiddenIndex - 1];
		const toShowInMenu = menuEntries[firstHiddenIndex - 1];
		$(toHideInBar).addClass("hidden-by-launcher");
		$(toShowInMenu).removeClass("in-header");
		$entryToInsertInBar.removeClass("hidden");
		$entryToInsertInBar.removeClass("hidden-by-launcher");
		$entryToInsertInMenu.addClass("in-header");
	}

	if (barToMenu) {
		const toShowInBar = barEntries[firstHiddenIndex];
		const toHideInMenu = menuEntries[firstHiddenIndex];
		$(toShowInBar).removeClass("hidden");
		$(toShowInBar).removeClass("hidden-by-launcher");
		$(toHideInMenu).addClass("in-header");
		$entryToInsertInBar.addClass("hidden-by-launcher");
		$entryToInsertInMenu.removeClass("in-header");
	}

	if (oldIndex > newIndex) {
		$entryToInsertInBar.insertBefore($entryToShiftInBar);
		$entryToInsertInMenu.insertBefore($entryToShiftInMenu);
	} else {
		$entryToInsertInBar.insertAfter($entryToShiftInBar);
		$entryToInsertInMenu.insertAfter($entryToShiftInMenu);
	}
}

$(document).ready(function() {
	const bodyId = $("body").attr("id");

	if (bodyId === "body-user" || bodyId === "body-settings") {
		const launcherDiv = $("<div id='launcher-menu' style='display: none'>");
		/*
    		Append base div to header as appending to body directly not working for some
    		reason
		*/
		$("#header").append(launcherDiv);

		mountLauncherComponent();

		const appMenu = $("#appmenu");
		if (!appMenu.length) return;

		appMenu.css("opacity", "0");
		// restore existing order
		axios
			.get(generateUrl("/apps/ecloud-launcher/getOrder"))
			.then(function(response) {
				let order = [];
				if (Array.isArray(response.data.order)) {
					order = response.data.order.reverse();
				}
				const hidden = [];

				mapMenu($("#appmenu"), order, hidden);
				mapMenu($("#apps").find("ul"), order, hidden);

				$(window).trigger("resize");
				appMenu.css("opacity", "1");
			});

		$(window).resize(function(e) {
			if (window.innerWidth > 650) {
				showNavbarEntries(7);
			} else {
				showNavbarEntries(0);
			}
		});

		document.addEventListener("navOrderUpdated", function(e) {
			const orderChange = e.detail;
			const barEntries = $("#header #appmenu > li");
			const menuEntries = $("#header #navigation #apps ul > li");
			insertEntry(
				barEntries,
				menuEntries,
				orderChange.oldIndex,
				orderChange.newIndex
			);
		});

		// Add heading to logo
		$("#nextcloud").append(
			"<div id='launcher-heading' class='vue-tooltip'><span class='tooltip-arrow'></span>" +
				"<span class='tooltip-inner'>" +
				OC.L10N.translate("launcher", "Apps") +
				"</span>" +
				"</div>"
		);

		$("#nextcloud").click(function(e) {
			e.preventDefault();
			if (!$("#launcher-menu").is(":visible")) {
				e.stopPropagation();
				showLauncher();
			}
		});

		$("body").click(function(e) {
			// In case launcher is clicked, don't close
			if ($(e.target).closest("#launcher-menu").length) {
				return;
			}
			// Hide launcher if it isn't hidden
			if (!$("#launcher-menu").is(":hidden")) {
				hideLauncher();
			}
		});

		$("body").on("keydown", function(e) {
			if (e.keyCode === 27) {
				hideLauncher();
			}
		});
	}
});
