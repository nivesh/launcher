OC.L10N.register(
    "ecloud-launcher",
    {
        "Apps": "Apps",
		"More Apps": "Plus d'applications",
		"Less Apps": "Moins d'applications",
		"untitled": "sans titre",
		"Error when trying to connect to ONLYOFFICE": "Erreur lors de la tentative de connexion à ONLYOFFICE"
},
"nplurals=2; plural=(n != 1);");
