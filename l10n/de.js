OC.L10N.register(
    "ecloud-launcher",
    {
    "Apps": "Apps",
    "More Apps": "Mehr Apps",
    "Less Apps": "Weniger Apps",
    "untitled": "unbetitelt",
    "Error when trying to connect to ONLYOFFICE": "Fehler beim Versuch, eine Verbindung zu ONLYOFFICE herzustellen"
},
"nplurals=2; plural=(n != 1);");
