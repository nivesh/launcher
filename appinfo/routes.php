<?php

return [
    'routes' => [
        ['name' => 'apps#index', 'url' => '/apps', 'verb' => 'GET'],
        ['name' => 'apps#getOrder', 'url' => '/getOrder', 'verb' => 'GET'],
        ['name' => 'apps#getDocumentsFolder', 'url' => '/getDocumentsFolder', 'verb' => 'GET'],
        ['name' => 'apps#updateOrder', 'url' => '/updateOrder', 'verb' => 'PUT']
    ]
];
