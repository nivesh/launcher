<?php

if (\OC::$server->getUserSession()->isLoggedIn()) {
    \OCP\Util::addScript('ecloud-launcher', 'launcher');
    \OCP\Util::addStyle('ecloud-launcher', 'main');
}
